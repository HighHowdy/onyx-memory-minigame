local MinigameActive = false
local MinigameFinished = false
local SuccessTrigger = nil
local FailTrigger = nil
local Success = false

-- Example use
RegisterCommand('testminigame', function()
    local success = StartMinigame({
        maxFails = 3,
        maxSquares = 15,
        rows = 7,
    })

    print(success) -- true/false
end)

-- RegisterNetEvent('test:success')
-- AddEventHandler('test:success', function()
--     print('success')
-- end)

-- RegisterNetEvent('test:fail')
-- AddEventHandler('test:fail', function()
--     print('fail')
-- end)

function StartMinigame(data, cb)
    if MinigameActive then return end

    if data ~= nil then
        local rows = 7

        if data.rows then rows = data.rows end

        SetNuiFocus(true, true)
        SendNUIMessage({action = 'start', fails = data.maxFails, squares = data.maxSquares, rows = rows})
        MinigameActive = true
        MinigameFinished = false

        while MinigameActive do
            Citizen.Wait(500)
        end

        if cb then
            cb(Success)
        end

        return Success
    end
end

exports('StartMinigame', StartMinigame)

RegisterNUICallback('success', function(data, cb)
    SetNuiFocus(false, false)
    Success = true
    MinigameFinished = false
    MinigameActive = false
    cb('ok')
end)

RegisterNUICallback('fail', function(data, cb)
    SetNuiFocus(false, false)
    MinigameActive = false
    Success = false
    cb('ok')
end)